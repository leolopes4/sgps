package br.com.pucminas.boasaude.sgps.controller;

import br.com.pucminas.boasaude.sgps.service.FinanceReportService;
import br.com.pucminas.boasaude.sgps.service.IdentifierCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class FinanceReportController {

    private FinanceReportService financeReportService;

    @Autowired
    FinanceReportController(FinanceReportService financeReportService){
        this.financeReportService = financeReportService;
    }
}
