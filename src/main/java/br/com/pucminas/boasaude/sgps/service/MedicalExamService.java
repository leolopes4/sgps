package br.com.pucminas.boasaude.sgps.service;

import br.com.pucminas.boasaude.sgps.integration.InfoRegisterIntegrationService;
import br.com.pucminas.boasaude.sgps.model.ProviderModelDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentRequestDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentResponseDTO;
import br.com.pucminas.boasaude.sgps.model.entities.AppointmentEntity;
import br.com.pucminas.boasaude.sgps.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MedicalExamService {

    private InfoRegisterIntegrationService infoRegisterIntegrationService;
    private AppointmentRepository appointmentRepo;

    @Autowired
    MedicalExamService(InfoRegisterIntegrationService infoRegisterIntegrationService, AppointmentRepository appointmentRepo){
        this.infoRegisterIntegrationService = infoRegisterIntegrationService;
        this.appointmentRepo = appointmentRepo;
    }

    public ResponseEntity<List<ProviderModelDTO>> getDoctorByLocationAndSpecialization(String authorization, String specialization, String location){
        ResponseEntity<List<ProviderModelDTO>> response = this.infoRegisterIntegrationService.getAllProviderBySpecialization(authorization, specialization, location);

        if (isSuccessRequest(response)) {
           return response;
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }


    }

    public ResponseEntity<List<AppointmentResponseDTO>> getAppointmentsByDoctorDocument(String doctorDocument){
        Optional<List<AppointmentEntity>> appointmentList = this.appointmentRepo.findByDoctorDocumentID(doctorDocument);

        List<AppointmentResponseDTO> appointmentResponseDTO = new ArrayList<AppointmentResponseDTO>();

        if(appointmentList.isEmpty()){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
            appointmentList.get().forEach(appointment -> {
                appointmentResponseDTO.add(appointment.toResponseDTO());
            });
        }

        return ResponseEntity.ok().body(appointmentResponseDTO);


    }


    public ResponseEntity<AppointmentResponseDTO> scheduleMedicalExam(AppointmentRequestDTO appointmentRequestDTO, UriComponentsBuilder uriBuilder){

        AppointmentEntity appointmentEntity = appointmentRequestDTO.toEntity();
        try {
            var appointmentEntitySaved = this.appointmentRepo.save(appointmentEntity);
            URI uri = uriBuilder.path("api/v1/appointment/{id}").buildAndExpand(appointmentEntity.getId()).toUri();
            var response = appointmentEntitySaved.toResponseDTO();

            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }

    public ResponseEntity<?> updateAppointment(Long id, Map<Object, Object> fields){
        var appointmentEntity = this.appointmentRepo.findById(id);

        if(appointmentEntity.isPresent()){

            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(AppointmentEntity.class, (String) key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, appointmentEntity.get(), value);
            });

            AppointmentEntity appointmentEntityUpdated = appointmentEntity.get();

            appointmentEntity.get().update(appointmentEntityUpdated);

            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    public boolean isSuccessRequest(ResponseEntity<?> response) {
        if (HttpStatus.OK.value() == response.getStatusCodeValue()) {
            return true;
        } else {
            return false;
        }
    }
}
