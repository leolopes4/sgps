package br.com.pucminas.boasaude.sgps.model.entities;


import br.com.pucminas.boasaude.sgps.model.enums.PlanTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "IdentifierCard")
@NoArgsConstructor
@RequiredArgsConstructor
public class IdentifierCardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String name;

    private LocalDate createdDate = LocalDate.now();
    private LocalDate expirationDate = createdDate.plusYears(3);

    @NonNull
    private String plan_type;

    @NonNull
    private String status;

    @NonNull
    private String document;

    public void update(IdentifierCardEntity newIdentifierCardEntity) {
        this.name = newIdentifierCardEntity.getName();
        this.createdDate = newIdentifierCardEntity.getCreatedDate();
        this.expirationDate = newIdentifierCardEntity.getExpirationDate();
        this.plan_type = newIdentifierCardEntity.getPlan_type();
        this.status = newIdentifierCardEntity.getStatus();
        this.document = newIdentifierCardEntity.getDocument();
    }



}
