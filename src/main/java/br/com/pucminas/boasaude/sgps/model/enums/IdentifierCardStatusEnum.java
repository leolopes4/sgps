package br.com.pucminas.boasaude.sgps.model.enums;

public enum IdentifierCardStatusEnum {

    PENDING("pending"),
    ISSUED("emitido");

    private String description;

    IdentifierCardStatusEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
