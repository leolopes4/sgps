package br.com.pucminas.boasaude.sgps.model.enums;

public enum AppointmentStatusEnum {

    SCHEDULED("scheduled"),
    FINISHED("finished"),
    CANCELLED("cancelled");

    private String description;

    AppointmentStatusEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
