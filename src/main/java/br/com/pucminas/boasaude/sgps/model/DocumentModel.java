package br.com.pucminas.boasaude.sgps.model;

import lombok.Data;

@Data
public class DocumentModel {

    private String document;
}
