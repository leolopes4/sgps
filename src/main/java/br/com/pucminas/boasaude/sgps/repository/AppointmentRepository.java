package br.com.pucminas.boasaude.sgps.repository;

import br.com.pucminas.boasaude.sgps.model.entities.AppointmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Long> {

    Optional<List<AppointmentEntity>> findByDoctorDocumentID(String documentID);
}
