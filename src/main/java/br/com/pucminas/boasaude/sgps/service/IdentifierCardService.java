package br.com.pucminas.boasaude.sgps.service;

import br.com.pucminas.boasaude.sgps.integration.InfoRegisterIntegrationService;
import br.com.pucminas.boasaude.sgps.model.ClientModel;
import br.com.pucminas.boasaude.sgps.model.entities.IdentifierCardEntity;
import br.com.pucminas.boasaude.sgps.model.enums.PlanTypeEnum;
import br.com.pucminas.boasaude.sgps.model.enums.IdentifierCardStatusEnum;
import br.com.pucminas.boasaude.sgps.repository.IdentifierCardRepository;
import org.aspectj.util.Reflection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Map;

@Service
public class IdentifierCardService {

    private InfoRegisterIntegrationService infoRegisterIntegrationService;
    private IdentifierCardRepository identifierRepo;

    @Autowired
    IdentifierCardService(InfoRegisterIntegrationService infoRegisterIntegrationService, IdentifierCardRepository identifierRepo) {
        this.infoRegisterIntegrationService = infoRegisterIntegrationService;
        this.identifierRepo = identifierRepo;
    }

    public void emitIdentifierCard(String authorization, String document) {
        var response = this.infoRegisterIntegrationService.verifyIfAssociateOrProviderExist(authorization, document);

        if (isSuccessRequest(response)) {
            ClientModel client = response.getBody();

            IdentifierCardEntity identifierCardEntity = new IdentifierCardEntity(client.getFirstName(), PlanTypeEnum.APARTAMENT.getDescription(), IdentifierCardStatusEnum.PENDING.getDescription(), document);

            this.identifierRepo.save(identifierCardEntity);

        } else {

        }


    }

    public ResponseEntity<?> updateIdentifierCard(String id, Map<Object, Object> fields){
        var identifierCardEntity = this.identifierRepo.findByDocument(id);

        if(identifierCardEntity.isPresent()){

            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(IdentifierCardEntity.class, (String) key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, identifierCardEntity.get(), value);
            });

            IdentifierCardEntity identifierCardEntityUpdated = identifierCardEntity.get();

            identifierCardEntity.get().update(identifierCardEntityUpdated);

            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    public boolean isSuccessRequest(ResponseEntity<?> response) {
        if (HttpStatus.OK.value() == response.getStatusCodeValue()) {
            return true;
        } else {
            return false;
        }
    }

}
