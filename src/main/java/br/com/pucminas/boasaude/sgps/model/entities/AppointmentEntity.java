package br.com.pucminas.boasaude.sgps.model.entities;

import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentRequestDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentResponseDTO;
import br.com.pucminas.boasaude.sgps.model.enums.AppointmentStatusEnum;
import br.com.pucminas.boasaude.sgps.model.enums.IdentifierCardStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "Appointment")
public class AppointmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String specialization;

    @NonNull
    private String doctorDocumentID;

    @NonNull
    private LocalDate scheduleDate;

    @NonNull
    private String location;

    @NonNull
    private String status;

    public void update(AppointmentEntity newAppointmentEntity) {
        this.specialization = newAppointmentEntity.getSpecialization();
        this.doctorDocumentID = newAppointmentEntity.getDoctorDocumentID();
        this.scheduleDate = newAppointmentEntity.getScheduleDate();
        this.location = newAppointmentEntity.getLocation();
        this.status = newAppointmentEntity.getStatus();
    }

    public AppointmentRequestDTO toDTO(){
        return new AppointmentRequestDTO(this.specialization, this.doctorDocumentID, this.scheduleDate, this.location);
    }

    public AppointmentResponseDTO toResponseDTO(){
        return new AppointmentResponseDTO(this.id, this.specialization, this.doctorDocumentID, this.scheduleDate, this.location, this.status);
    }
}
