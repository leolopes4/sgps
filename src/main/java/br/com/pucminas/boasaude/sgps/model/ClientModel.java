package br.com.pucminas.boasaude.sgps.model;

import lombok.Data;

@Data
public class ClientModel {

    private String firstName;
    private String lastName;
    private String document;
    private String email;
}
