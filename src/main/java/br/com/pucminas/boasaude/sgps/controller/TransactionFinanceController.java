package br.com.pucminas.boasaude.sgps.controller;


import br.com.pucminas.boasaude.sgps.service.TransactionFinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class TransactionFinanceController {

    private TransactionFinanceService transactionFinanceService;

    @Autowired
    TransactionFinanceController(TransactionFinanceService transactionFinanceService){
        this.transactionFinanceService = transactionFinanceService;
    }
}

