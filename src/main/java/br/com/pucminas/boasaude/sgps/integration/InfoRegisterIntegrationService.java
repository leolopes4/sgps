package br.com.pucminas.boasaude.sgps.integration;

import br.com.pucminas.boasaude.sgps.integration.client.InfoRegisterClient;
import br.com.pucminas.boasaude.sgps.model.ClientModel;
import br.com.pucminas.boasaude.sgps.model.ProviderModelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InfoRegisterIntegrationService {

    private InfoRegisterClient infoRegisterClient;

    @Autowired
    InfoRegisterIntegrationService(InfoRegisterClient infoRegisterClient){
        this.infoRegisterClient = infoRegisterClient;
    }

    public ResponseEntity<ClientModel> verifyIfAssociateOrProviderExist(String authorization, String document){
        return infoRegisterClient.hadAssociateOrProvider(authorization, document);
    }

    public ResponseEntity<List<ProviderModelDTO>> getAllProviderBySpecialization(String authorization, String specialization, String location){
        return infoRegisterClient.getAllProviderBySpecialization(authorization, specialization, location);
    }

}

