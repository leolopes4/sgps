package br.com.pucminas.boasaude.sgps.model.enums;

public enum PlanTypeEnum {

    VIP("vip"),
    CO_PARTICIPATION("co-particicao"),
    APARTAMENT("apartamento"),
    DENTAL("dental");

    private String description;

    PlanTypeEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
