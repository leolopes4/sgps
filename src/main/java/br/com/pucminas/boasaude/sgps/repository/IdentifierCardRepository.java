package br.com.pucminas.boasaude.sgps.repository;

import br.com.pucminas.boasaude.sgps.model.entities.IdentifierCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IdentifierCardRepository extends JpaRepository<IdentifierCardEntity, Long> {

    Optional<IdentifierCardEntity> findByDocument(String document);
}
