package br.com.pucminas.boasaude.sgps.controller;

import br.com.pucminas.boasaude.sgps.model.DocumentModel;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Map;

@Api(tags = "IdentifierCardDocumentation")
public interface APIIdentifierCardController {


    @ApiOperation(value = "Issue Identifier Card")
    @ApiImplicitParam(name = "Authorization", value = "Bearer Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer emB9fS8AV4b8xf6jIZzZQD3e1NOyLMWpd9wuMyj4BnpXEDkd8rEgB6ONtIeUH7MB")
    public void emitIdentifierCard( String authorization, DocumentModel documentModel);

    @ApiOperation(value = "Update Identifier Card")
    public ResponseEntity<?> updateIdentifierCard(@ApiParam(value = "id", required = true) String id, Map<Object, Object> fields);
}
