package br.com.pucminas.boasaude.sgps.model.dto.request;

import br.com.pucminas.boasaude.sgps.model.enums.AppointmentStatusEnum;
import br.com.pucminas.boasaude.sgps.model.enums.IdentifierCardStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class AppointmentResponseDTO {

    private Long id;
    private String specialization;
    private String doctorDocumentID;
    private LocalDate scheduleDate;
    private String location;
    private String status;
}
