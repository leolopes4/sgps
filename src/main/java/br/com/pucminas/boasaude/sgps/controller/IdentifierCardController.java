package br.com.pucminas.boasaude.sgps.controller;

import br.com.pucminas.boasaude.sgps.model.DocumentModel;
import br.com.pucminas.boasaude.sgps.service.IdentifierCardService;
import br.com.pucminas.boasaude.sgps.service.TransactionFinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class IdentifierCardController implements APIIdentifierCardController {


    private IdentifierCardService identifierCardService;

    @Autowired
    IdentifierCardController(IdentifierCardService identifierCardService){
        this.identifierCardService = identifierCardService;
    }

    @PostMapping("/identifier-card")
    public void emitIdentifierCard(@RequestHeader("authorization") String authorization, @RequestBody DocumentModel documentModel){
        this.identifierCardService.emitIdentifierCard(authorization, documentModel.getDocument());

    }

    @PatchMapping("/identifier-card/{id}")
    @Transactional
    public ResponseEntity<?> updateIdentifierCard(@PathVariable String id, @RequestBody Map<Object, Object> fields){
        return this.identifierCardService.updateIdentifierCard(id, fields);

    }
}

