package br.com.pucminas.boasaude.sgps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgpsApplication.class, args);
	}

}
