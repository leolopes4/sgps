package br.com.pucminas.boasaude.sgps.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProviderModelDTO {

    private String firstName;
    private String lastName;
    private String document;
    private String email;
    private String specialization;
}
