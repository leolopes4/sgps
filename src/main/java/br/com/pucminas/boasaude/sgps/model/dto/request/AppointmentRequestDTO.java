package br.com.pucminas.boasaude.sgps.model.dto.request;

import br.com.pucminas.boasaude.sgps.model.entities.AppointmentEntity;
import br.com.pucminas.boasaude.sgps.model.enums.AppointmentStatusEnum;
import br.com.pucminas.boasaude.sgps.model.enums.IdentifierCardStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class AppointmentRequestDTO {

    private String specialization;
    private String doctorDocumentID;
    private LocalDate scheduleDate;
    private String location;

    public AppointmentEntity toEntity(){
        return new AppointmentEntity(this.specialization, this.doctorDocumentID, this.scheduleDate, this.location, AppointmentStatusEnum.SCHEDULED.getDescription());
    }
}
