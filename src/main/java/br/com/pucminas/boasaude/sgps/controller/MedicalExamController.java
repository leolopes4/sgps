package br.com.pucminas.boasaude.sgps.controller;

import br.com.pucminas.boasaude.sgps.model.ProviderModelDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentRequestDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentResponseDTO;
import br.com.pucminas.boasaude.sgps.service.MedicalExamService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class MedicalExamController implements APIMedicalExamController {


    private MedicalExamService medicalExamService;

    MedicalExamController(MedicalExamService medicalExamService){
        this.medicalExamService = medicalExamService;
    }

    @GetMapping("/appointment/{specialization}/{location}")
    public ResponseEntity<List<ProviderModelDTO>> getDoctorByLocationAndSpecialization(@RequestHeader("authorization") String authorization, @PathVariable String specialization, @PathVariable String location){
       return this.medicalExamService.getDoctorByLocationAndSpecialization(authorization, specialization, location);
    }

    @GetMapping("/appointment/{doctorDocumentId}")
    public ResponseEntity<List<AppointmentResponseDTO>> getAppointmentsByDoctorDocument(@PathVariable String doctorDocumentId){
        return this.medicalExamService.getAppointmentsByDoctorDocument(doctorDocumentId);
    }

    @PostMapping("/appointment")
    public ResponseEntity<AppointmentResponseDTO> scheduleMedicalExam(@RequestBody AppointmentRequestDTO appointmentRequestDTO, UriComponentsBuilder uriBuilder ){
        return this.medicalExamService.scheduleMedicalExam(appointmentRequestDTO, uriBuilder);
    }

    @PatchMapping("/appointment/{id}")
    @Transactional
    public ResponseEntity<?> updateAppointment(@PathVariable Long id, @RequestBody Map<Object, Object> fields){
        return this.medicalExamService.updateAppointment(id, fields);

    }
}
