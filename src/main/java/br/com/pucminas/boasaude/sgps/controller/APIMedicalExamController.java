package br.com.pucminas.boasaude.sgps.controller;

import br.com.pucminas.boasaude.sgps.model.ProviderModelDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentRequestDTO;
import br.com.pucminas.boasaude.sgps.model.dto.request.AppointmentResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Api(tags = "MedicalExamDocumentation")
public interface APIMedicalExamController {

    @ApiOperation(value = "Appointment Seacher")
    @ApiImplicitParam(name = "Authorization", value = "Bearer Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer emB9fS8AV4b8xf6jIZzZQD3e1NOyLMWpd9wuMyj4BnpXEDkd8rEgB6ONtIeUH7MB")
    public ResponseEntity<List<ProviderModelDTO>> getDoctorByLocationAndSpecialization(String authorization, @ApiParam(value = "specialization", required = true) String specialization, @ApiParam(value = "location", required = true) String location);

    @ApiOperation(value = "Appointment by Doctor")
    public ResponseEntity<List<AppointmentResponseDTO>> getAppointmentsByDoctorDocument(@ApiParam(value = "doctorDocumentId", required = true) String doctorDocumentId);

    @ApiOperation(value = "Schedule new Appointment")
    public ResponseEntity<AppointmentResponseDTO> scheduleMedicalExam(AppointmentRequestDTO appointmentRequestDTO, UriComponentsBuilder uriBuilder );

    @ApiOperation(value = "Update Appointment")
    public ResponseEntity<?> updateAppointment(@ApiParam(value = "id", required = true) Long id, Map<Object, Object> fields);

}
