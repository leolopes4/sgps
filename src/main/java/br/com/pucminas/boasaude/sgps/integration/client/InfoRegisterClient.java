package br.com.pucminas.boasaude.sgps.integration.client;

import br.com.pucminas.boasaude.sgps.configuration.FeignConfiguration;
import br.com.pucminas.boasaude.sgps.model.ClientModel;
import br.com.pucminas.boasaude.sgps.model.ProviderModelDTO;
import feign.Headers;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "InfoRegisterIntegration", url = "${integration.info.register.url}", configuration = FeignConfiguration.class)
public interface InfoRegisterClient {

    @GetMapping("/client?document={document}")
    public ResponseEntity<ClientModel> hadAssociateOrProvider(@RequestHeader("authorization") String authorization, @PathVariable(name = "document") String document);

    @GetMapping("/providers/{specialization}/{location}")
    public ResponseEntity<List<ProviderModelDTO>> getAllProviderBySpecialization(@RequestHeader("authorization") String authorization, @PathVariable(name = "specialization") String specialization, @PathVariable(name = "location") String location);
}